const nombres = document.getElementById("minombre");
const apellidos = document.getElementById("miapellido");
const email = document.getElementById("email");
const telefono = document.getElementById("telefono");
const contrasenia = document.getElementById("password");
const contrasenia2 = document.getElementById("repeatPassword");
const fechanacimiento = document.getElementById("fechanacimiento");
const terminosYcondiciones = document.getElementById("terminosycondiciones");
const formulario = document.getElementById("form");
const listInsputs = document.querySelectorAll(".form-input");

form.addEventListener("submit", (e) => {
    e.preventDefault();
    let condicion = validaciondatos();
    if(condicion){
        enviarFormulario();
    }
});

function validaciondatos() {
    formulario.lastElementChild.innerHTML = "";
    let condicion = true;
    listInsputs.forEach(element => {
        element.lastElementChild.innerHTML= "";
    });

    if(nombres.value.lenght < 1 || nombres.value.trim() == "") {
        muestraError("minombre", "Nombres no válidos*");
        condicion = false; 
    }
    if(apellidos.value.lenght < 1 || apellidos.value.trim() == "") {
        muestraError("miapellido", "Apellidos no válidos*");
        condicion = false; 
    }
    if(email.value.lenght < 1 || email.value.trim() == "") {
        muestraError("email", "Correo electrónico no válido*");
        condicion = false; 
    }
    if(telefono.value.lenght < 10 || telefono.value.trim() == "" || isNaN(telefono.value)) {
        muestraError("telefono", "Teléfono no válido*");
        condicion = false; 
    }
    if(contrasenia.value.lenght < 1 || contrasenia.value.trim() == "") {
        muestraError("password", "Contraseña no válida*");
        condicion = false; 
    }
    if(contrasenia2.value != contrasenia.value) {
        muestraError("repeatPassword", "Confirmación no válida*");
        condicion = false; 
    }
    if(!terminosYcondiciones.checked) {
        muestraError("terminosycondiciones", "Acepte para continuar*");
        condicion = false; 
    } else {muestraError("terminosycondiciones", ""); }
    return condicion;
}

function muestraError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
}
function enviarFormulario(){
    formulario.reset();
    formulario.lastElementChild.innerHTML = "¡Registro Exitoso!";
}